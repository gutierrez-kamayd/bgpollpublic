//  OpenShift sample Node application
var express = require('express'),
  app = express(),
  morgan = require('morgan');

const ObjectId = require('mongodb').ObjectId;

Object.assign = require('object-assign')
app.engine('html', require('ejs').renderFile);
app.use(morgan('combined'));
app.use(express.urlencoded())
app.use(express.static('public'))

//react to events like team join

const runClubChannelId = 'CB34Q10TG';
const soccerTeamChannelId = 'C9DE3EF7W';
const myPersonalChannelId = 'DB933RNJD';
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
  ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
  mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
  mongoURLLabel = "";

if (mongoURL == null) {
  var mongoHost, mongoPort, mongoDatabase, mongoPassword, mongoUser;
  // If using plane old env vars via service discovery
  if (process.env.DATABASE_SERVICE_NAME) {
    var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase();
    mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'];
    mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'];
    mongoDatabase = process.env[mongoServiceName + '_DATABASE'];
    mongoPassword = process.env[mongoServiceName + '_PASSWORD'];
    mongoUser = process.env[mongoServiceName + '_USER'];

    // If using env vars from secret from service binding  
  } else if (process.env.database_name) {
    mongoDatabase = process.env.database_name;
    mongoPassword = process.env.password;
    mongoUser = process.env.username;
    var mongoUriParts = process.env.uri && process.env.uri.split("//");
    if (mongoUriParts.length == 2) {
      mongoUriParts = mongoUriParts[1].split(":");
      if (mongoUriParts && mongoUriParts.length == 2) {
        mongoHost = mongoUriParts[0];
        mongoPort = mongoUriParts[1];
      }
    }
  }

  if (mongoHost && mongoPort && mongoDatabase) {
    mongoURLLabel = mongoURL = 'mongodb://';
    if (mongoUser && mongoPassword) {
      mongoURL += mongoUser + ':' + mongoPassword + '@';
    }
    // Provide UI label that excludes user id and pw
    mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
    mongoURL += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
  }
}
var db = null,
  dbDetails = new Object();

const getDb = () => (new Promise((resolve, reject) => {
  if (mongoURL == null) {
    reject('no mongo url');
    return;
  }

  var mongodb = require('mongodb');
  if (mongodb == null) {
    reject('unable to require mongodb');
    return;
  }
  if (db) {
    resolve(db);
  }
  else {
    mongodb.connect(mongoURL, function (err, conn) {
      if (err) {
        reject(err);
        return;
      }

      db = conn;
      dbDetails.databaseName = db.databaseName;
      dbDetails.url = mongoURLLabel;
      dbDetails.type = 'MongoDB';

      console.log('Connected to MongoDB at: %s', mongoURL);
      resolve(db);
    });
  }
}));

app.get('/', (req, res) => {
  res.render('index.html', { message: "The message" });
});


const getDefaultQuestion = (channel_id, messageDate) => {
  switch (channel_id) {
    case runClubChannelId:
    case myPersonalChannelId:
      {
        let weekDay = new Date().toLocaleString('en-US', { timeZone: "America/New_York" ,  weekday: 'long'});
        if(weekDay === 'Tuesday')
        {
          return '*Are you running today*:question:\n*Rendezvous:* Parking lot across the street @ `7:00pm`';
        }
        return '*Are you running today*:question:\n*Rendezvous:* Downstairs at Vapiano @ `6:00pm`';
      }
    case soccerTeamChannelId: {
      return '*Are you playing today*:question:\n*Rendezvous:* Brickell Cage from `6:00pm - 7:30pm`';
    }
  }
  return '';
};

//TODO: implement something that if not format is passed convert the question into bold letters
const boldTextIfNotFormated = (text)=>{

};

//TODO: empty command call should give you explanation with options
//TODO: allow for more than true all false, same as simple poll, parsing the text is the only hard thing to do, rest should be to break the line for the buttons.
//TODO: Hero presentation you like must.
/*3 Characters you must encode as HTML entities
There are three characters you must convert to HTML entities and only three: &, <, and >.

Slack will take care of the rest.

Replace the ampersand, &, with &amp;
Replace the less-than sign, < with &lt;
Replace the greater-than sign, > with &gt;
*/

const teamDistribution = (playerCount)=>{
  if(playerCount<=0) return '';
  if(playerCount===1) return '';
  if(playerCount===2) return ' :one::vs::one:';
  if(playerCount===3) return ' :one::vs::two:';
  if(playerCount===4) return ' :two::vs::two:';
  if(playerCount===5) return ' :two::vs::three:';
  if(playerCount===6) return ' :three::vs::three:';
  if(playerCount===7) return ' :three::vs::four:';
  if(playerCount===8) return ' :four::vs::four:';
  if(playerCount===9) return ' :four::vs::five:';
  if(playerCount===10) return ' :five::vs::five:';
  if(playerCount===11) return ' :five::vs::six:';
  if(playerCount===12) return ' :six::vs::six:';
  return ` :six::vs::six: (${playerCount-12} rotating)`
};

const priceFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
});

const pricePerPlayer = (playerCount)=>{
  if(playerCount)
  {
    return "\nprice per player: `" + priceFormatter.format(120.00/playerCount) + "` _(for amount of players below)_" ;
  }
  return '';
};

//to handle the command call
app.post('/', async (req, res) => {
  const { text, user_id, trigger_id, user_name, response_url, channel_id } = req.body;
  //console.log('req.body',JSON.stringify(req.body));
  let messageDate = new Date();
  console.log('messageDate',messageDate);
  let created = `<!here|here> _a quick question from_ <@${user_id}>`;
  let question = text || getDefaultQuestion(channel_id, messageDate);
  let db = await getDb();
  let polls = db.collection('poll');
  let poll = await polls.insert({ users: [[], []] });
  let callback_id = poll.insertedIds[0];
  res.status(200).send({
    "response_type": "in_channel",
    "text": created,
    "attachments": [
      {
        "text": question,
        "fallback": "You are unable to vote",
        callback_id,
        "color": "#3AA3E3",
        "mrkdwn_in": ["pretext", "text", "fields"],
        "mrkdwn": true,
        "actions": [
          {
            "name": "yes",
            "text": ":heavy_check_mark:*YES*",
            "type": "button",
            "value": "0",
            "style": "primary",
          },
          {
            "name": "no",
            "text": ":x:*NO*",
            "type": "button",
            "value": "1",
            "style": "danger",
          }
        ]
      },
      {
        "text": ":point_up_2:*Be first to answer*",
        "color": "#3AA3E3",
        "mrkdwn": true,
        "mrkdwn_in": ["text"]
      }
    ]
  });
});

const addUserAttachment = ()=>{
  return {
    "attachments": [
        {
            "callback_id": "select_simple_1234",
            "actions": [
                {
                    "name": "games_list",
                    "text": "Pick a game...",
                    "type": "select",
                    "options": [
                        {
                            "text": "Chess",
                            "value": "chess"
                        },
                        {
                            "text": "Falken's Maze",
                            "value": "maze"
                        },
                        {
                            "text": "Thermonuclear War",
                            "value": "war"
                        }
                    ],
                    "selected_options": [
                        {
                            "text": "Falken's Maze",
                            "value": "maze"
                        }
                    ]
                }
            ]
        }
    ]
}
}

const getDefaultAttachmentsForYesOrNo = (channel_id, question, users, messageDate) => {
  let yesUsers = users[0];
  let noUsers = users[1];
  let yesUserText = yesUsers.join(', ');
  let noUsersText = noUsers.join(', ');
  if (yesUserText) {
    yesUserText = '\n' + yesUserText;
  }
  if (noUsersText) {
    noUsersText = '\n' + noUsersText;
  }

  if ((channel_id === runClubChannelId || channel_id === myPersonalChannelId) && getDefaultQuestion(channel_id, messageDate) === question) {
    return [{
      "text": ":heavy_check_mark:*YES* :gottarun: `" + yesUsers.length + "`" + (yesUserText || ' No runners listed yet. *Be First*!'),
      "color": "#00ff00",
      "mrkdwn": true,
      "mrkdwn_in": ["text"],
      "image_url": `http://nodejs-mongo-persistent-bgpoll.a3c1.starter-us-west-1.openshiftapps.com/run${Math.min(20,yesUsers.length)}.gif`
    },
    {
      "text": ":x:*NO*  :kamayd-rick3: `" + noUsers.length + "`" + (noUsersText || ' Please let us know if you cannot make it.'),
      "color": "#ff0000",
      "mrkdwn": true,
      "mrkdwn_in": ["text"]
    }];
  }
  else if ((channel_id === soccerTeamChannelId || channel_id === myPersonalChannelId) && getDefaultQuestion(channel_id, messageDate) === question) {
    let teamDistributionText = teamDistribution(yesUsers.length);
    let pricePerPlayerText = pricePerPlayer(yesUsers.length);
    return [{
      "text": ":heavy_check_mark:*YES* :soccer: `" + yesUsers.length + "`" + teamDistributionText + pricePerPlayerText + (yesUserText || ' No players listed yet. *Be First*!'),
      "color": "#00ff00",
      "mrkdwn": true,
      "mrkdwn_in": ["text"],
      "image_url": `http://nodejs-mongo-persistent-bgpoll.a3c1.starter-us-west-1.openshiftapps.com/soccer${Math.min(20,yesUsers.length)}.png`
    },
    {
      "text": ":x:*NO*  :kamayd-rick3: `" + noUsers.length + "`" + (noUsersText || ' Please let us know if you cannot make it.'),
      "color": "#ff0000",
      "mrkdwn": true,
      "mrkdwn_in": ["text"]
    }];
  }
  return [{
    "text": ":heavy_check_mark:*YES* `" + yesUsers.length + "`" + (yesUserText || ' _empty_'),
    "color": "#00ff00",
    "mrkdwn": true,
    "mrkdwn_in": ["text"]
  },
  {
    "text": ":x:*NO* `" + noUsers.length + "`" + (noUsersText || ' _empty_'),
    "color": "#ff0000",
    "mrkdwn": true,
    "mrkdwn_in": ["text"]
  }];
}

//to handle the interaction
app.post('/interaction', async (req, res) => {
  let payload = JSON.parse(req.body.payload);
  const { callback_id, actions, user, channel, original_message, message_ts } = payload;
  let { value } = actions[0];
  value = Number(value);
  console.log('payload',JSON.stringify(payload));
  let messageDate = new Date(message_ts*1000);

  const userId = user ? user.id : null;
  const channelId = channel ? channel.id : null;
  let db = await getDb();
  let polls = db.collection('poll');
  const _id = new ObjectId(callback_id);
  let poll = await polls.findOne({ _id });
  let currentUser = `<@${userId}>`;
  for(let i=0; i<poll.users.length; i++)
  {
    if(i===value)
    {
      if (!poll.users[i].includes(currentUser))
      {
        poll.users[i].push(currentUser);
      }
    }
    else{
      poll.users[i] = poll.users[i].filter(item => item !== currentUser);
    }
  }

  await polls.updateOne({ _id }, { $set: { users: poll.users} });

  let question = original_message.attachments[0].text;
  let attachments = [ original_message.attachments[0] ].concat(getDefaultAttachmentsForYesOrNo(channelId, question, poll.users, messageDate));

  res.status(200).send({
    "response_type": "in_channel",
    "text": original_message.text,
    attachments
  });
});

// error handling
app.use((err, req, res, next)=> {
  console.error(err.stack);
  res.status(500).send('Something bad happened!');
});

getDb().catch((err) => console.log('Error connecting to Mongo. Message:\n' + err));

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app;
